import type { Employee } from './Employee'

type EmployeeCico = {
  id: number
  date: Date
  statusCheck: boolean
  employee: Employee
  checkIn: Date | null // Add this line
  checkOut: Date | null // Add this line
  clicked?: boolean
  checkInClicked?: boolean
  checkOutClicked?: boolean
  showClock: boolean
}

export type { EmployeeCico }
