import type { Order } from './Order'
import type { Product } from '../Product'

type OrderItems = {
  id: number

  name: string

  price: number

  qty: number

  total: number

  created: Date

  updated: Date

  order: Order

  product: Product
}

export type { OrderItems }
