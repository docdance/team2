import type { Product } from '../Product'

type ReceiptItem = {
  product: Product
  qty: number
}

export { type ReceiptItem }
