import type { Member } from '../Member'
import type { Promotion } from '../Promotion'
import type { User } from '../User'
import type { OrderItems } from './OrderItems'

type Payment = 'cash' | 'mastercard' | 'qr-code'

type Order = {
  id?: number
  orderItemsIdQty: {
    productId: number
    qty: number
  }[]
  created: Date
  total: number
  qty: number
  user?: User
  cash: number
  discount: number
  promotions: Promotion[]
  member: Member | null
  payment: Payment
  getPoint: number
  usePoint: number
  orderItems?: OrderItems[]
}

export type { Order }
