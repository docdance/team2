import type { Member } from '../Member'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from '../User'

type Payment = 'cash' | 'mastercard' | 'qr-code'

type Receipt = {
  id?: number

  total: number

  qty: number

  created: Date

  receiptItems: ReceiptItem[]

  user: User | null

  member: Member | null

  cash: number

  payment: Payment

  discount: number
}

export type { Receipt }
