type PromotionAdd = {
  name: string
  description: string
  start_date: string
  end_date: string
}

export type { PromotionAdd }
