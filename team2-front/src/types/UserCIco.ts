import type { User } from './User'

type UserCico = {
  id: number
  date: Date
  statusCheck: boolean
  user: User
  checkIn: Date | null // Add this line
  checkOut: Date | null // Add this line
  clicked?: boolean
  checkInClicked?: boolean
  checkOutClicked?: boolean
  showClock: boolean
}

export type { UserCico }
