type Vender = {
  id?: number
  name: string
  tel: string
  image?: string
}

export { type Vender }
