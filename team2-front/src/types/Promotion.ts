type Promotion = {
  id?: number
  name: string
  percentDiscount: number
  priceDiscount: number
  getPoint: number
  usePoint: number
  minQty: number
  minPrice: number
  member: boolean
  productId: number
  forOneItem: boolean
  description: string
  start_date: string
  end_date: string
}

export type { Promotion }
