import type { Role } from './Role'

type Gender = 'male' | 'female' | 'others'
type SalaryStatus = 'paid' | 'unpaid'
type User = {
  id?: number
  image: string
  email: string
  password: string
  fullName: string
  tel: string
  gender: Gender // Male, Female, Others
  roles: Role[] // admin, user
  baseSalary: number
  bankName: string
  bankAccount: string
  // salaryStatus: SalaryStatus
}

export type { Gender, User }
