type Status = 'Available' | 'Low'
type Material = {
  id?: number
  in_date: string
  name: string
  price: number
  qt_previous: number
  quantity: number
  use: number
  min: number

  status: Status
  // image: string
}
// function getImageUrl(material: Material) {
//   return `/img/material${material.id}.jpg`
// }

export type { Material }
