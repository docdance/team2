import type { Material } from './Materials'

type PurOrderDetail = {
  material: Material
  quantity: number
}

export { type PurOrderDetail }
