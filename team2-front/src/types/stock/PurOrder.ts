type PurOrder = {
  purOrderDetails: {
    materialId: number
    quantity: number
  }[]
  userId?: number
}

export type { PurOrder }
