import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '@/views/LoginView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/login' // Show login page first
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    },
    {
      path: '/pos',
      name: 'pos',
      components: {
        default: () => import('../views/pos/PosView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/home',
      name: 'home',
      component: {
        default: () => import('../views/HomeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      }
    },
    {
      path: '/stock',
      name: 'stock',
      components: {
        default: () => import('../views/stock/StockView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/report_stock',
      name: 'report_stock',
      components: {
        default: () => import('../views/stock/ReportStock.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/pur_order',
      name: 'pur_order',
      components: {
        default: () => import('../views/stock/purchaseOrder/PurchaseView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/users',
      name: 'users',
      components: {
        default: () => import('../views/UserView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/checkincheckout',
      name: 'checkincheckout',
      components: {
        default: () => import('../views/CheckInCheckOut.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/product',
      name: 'product',
      components: {
        default: () => import('../views/ProductView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/promotion',
      name: 'promotion',
      components: {
        default: () => import('../views/PromotionView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/order',
      name: 'order',
      components: {
        default: () => import('../views/order/OrderView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    }
  ]
})

function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  }
  return false
}

router.beforeEach((to, from) => {
  if (to.meta.requireAuth && !isLogin()) {
    router.replace('/login')

    // return {
    //   path: '/login',
    //   query: { redirect: to.fullPath }
    // }
  }
})

export default router
