import { defineStore } from 'pinia'

import type { CheckDetail } from '@/types/stock/CheckDetail'

import type { CheckMaterial } from '@/types/stock/CheckMaterials'
import { useAuthStore } from '../auth'

import { useLoadingStore } from '../loading'
import { useMessageStore } from '../message'
import stockService from '@/services/stocks/stock'
import { computed, ref, watch } from 'vue'
import { useMaterialStore } from './material'

export const useCheckStockStore = defineStore('checkStock', () => {
  // const _num = inject<Ref<number>>('num1', ) ?? ref(1)
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const materialStore = useMaterialStore()
  const checkStockDialog = ref(false)
  const searchQuery = ref<string>('')
  const checkStocks = ref<CheckMaterial[]>([])
  const checkStockDetail = ref<CheckDetail[]>([])

  const checkDetails = ref<CheckMaterial>({
    date: '',
    totalPrice: 0,
    totalQty: 0,
    totalUse: 0,
    userId: 1,
    userName: '',
    stockDetails: [],
    user: authStore.getCurrentUser()
  })

  const initialCheckStock: CheckMaterial = {
    date: '',
    totalPrice: 0,
    totalQty: 0,
    totalUse: 0,
    userName: '',
    userId: authStore.getCurrentUser()!.id!,
    stockDetails: [],
    user: authStore.getCurrentUser()
  }
  const editedCheckStock = ref<CheckMaterial>(JSON.parse(JSON.stringify(initialCheckStock)))

  watch(
    (checkStockDetail.value = []),
    () => {
      calCheckStock()
    },
    { deep: true }
  )
  const calCheckStock = function () {
    checkDetails.value!.totalPrice = 0
    checkDetails.value!.totalQty = 0
    checkDetails.value!.totalUse = 0
    for (let i = 0; i < checkStockDetail.value.length; i++) {
      checkDetails.value!.totalPrice +=
        checkStockDetail.value[i].price * checkStockDetail.value[i].quantity
      checkDetails.value!.totalQty += checkStockDetail.value[i].quantity
      checkDetails.value!.totalUse += checkStockDetail.value[i].use
    }
  }

  async function deleteStock() {
    loadingStore.doLoading()
    const stocks = editedCheckStock.value
    const res = await stockService.delStock(stocks)
    await getStocks()
    loadingStore.finish()
  }

  async function saveCheckStock() {
    try {
      loadingStore.doLoading()
      const stock = editedCheckStock.value
      const checkDetails = checkStockDetail.value
      if (!stock.id) {
        // add new
        console.log('post ' + JSON.stringify(stock))
        const res = await stockService.addStock(stock, checkDetails)
        // } else {
        //   // update
        //   console.log('patch ' + JSON.stringify(material))
        //   const res = await materialService.updateMaterial(material)
        // }
        await getStocks()
        loadingStore.finish()
      }
    } catch (e: any) {
      //  messageStore.showMessage(e.message)
      console.log('Error')
      loadingStore.finish()
    }
  }

  async function getStock(id: number) {
    try {
      loadingStore.doLoading()
      const res = await stockService.getStock(id)
      editedCheckStock.value = res.data

      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }
  async function getStocks() {
    try {
      loadingStore.doLoading()
      const res = await stockService.getStocks()
      checkStocks.value = res.data

      loadingStore.finish()
    } catch (e) {
      console.log('Error')

      loadingStore.finish()
    }
  }

  const addCheckStockDetail = async (stockInit: CheckDetail) => {
    stockInit = await materialStore.editedStockDetail
    checkStockDetail.value.push(stockInit)
  }
  // const deleteCheckStockDetail = (selectedDetail: CheckDetail) => {
  //   const index = checkStockDetails.value.findIndex((item) => item === selectedDetail)
  //   checkStockDetails.value.splice(index, 1)
  // }
  // const incUnitOfCheckStockDetail = (selectedDetail: CheckDetail) => {
  //   selectedDetail.unit++
  // }
  // const decUnitOfCheckStockDetail = (selectedDetail: CheckDetail) => {
  //   selectedDetail.unit--
  //   if (selectedDetail.unit === 0) {
  //     deleteCheckStockDetail(selectedDetail)
  //   }
  // }
  const removeDetail = (item: CheckDetail) => {
    const index = checkStockDetail.value.findIndex((ri) => ri === item)
    checkStockDetail.value.splice(index, 1)
  }
  const stock = async () => {
    try {
      loadingStore.doLoading()
      await stockService.addStock(checkDetails.value!, checkStockDetail.value)

      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  const filteredStock = computed(() => {
    const query = searchQuery.value.toLowerCase().trim()
    if (!query) {
      return checkStocks.value // Return all materials if search query is empty
    } else {
      return checkStocks.value.filter(
        (stock) => stock.date.toLowerCase().includes(query) || stock.date.toString().includes(query)
        // Add more fields to search as needed
      )
    }
  })

  function clearForm() {
    editedCheckStock.value = JSON.parse(JSON.stringify(initialCheckStock))
  }

  async function showStockDetial(id: number) {
    try {
      loadingStore.doLoading()
      const res = await stockService.getStock(id)
      editedCheckStock.value = res.data

      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }
  return {
    editedCheckStock,
    showStockDetial,
    checkStockDialog,
    deleteStock,
    clearForm,
    checkStockDetail,
    addCheckStockDetail,
    removeDetail,
    getStock,
    stock,
    saveCheckStock,
    filteredStock,
    getStocks,
    searchQuery
  }
})
