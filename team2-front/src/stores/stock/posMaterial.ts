import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from '../loading'
import materialService from '@/services/stocks/material'

import type { Material } from '@/types/stock/Materials'

export const usePosMatStore = defineStore('pos', () => {
  const loadingStore = useLoadingStore()
  const material1 = ref<Material[]>([])
  const material2 = ref<Material[]>([])

  async function getMaterials() {
    try {
      loadingStore.doLoading()
      const res = await materialService.getMaterials()
      material1.value = res.data
      // res = await productService.getProducts()
      // products2.value = res.data

      loadingStore.finish()
    } catch (e) {
      console.log('Error')

      loadingStore.finish()
    }
  }
  return { material1, getMaterials }
})
