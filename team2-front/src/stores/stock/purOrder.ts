import { computed, inject, ref, type Ref } from 'vue'
import { defineStore } from 'pinia'

import purOrderService from '@/services/stocks/purOrder'

import { useVenderStore } from '../vender'
import type { PurOrderDetail } from '@/types/stock/PurOrderDetail'
import type { PurOrderBill } from '@/types/stock/PurOrderBill'
import type { PurOrder } from '@/types/stock/PurOrder'

import type { Material } from '@/types/stock/Materials'
import { useAuthStore } from '../auth'
import { useLoadingStore } from '../loading'
import dayjs from 'dayjs'

export const usePurOrderStore = defineStore('purOrder', () => {
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const venderStore = useVenderStore()
  const purOrderDialog = ref(false)
  const purOrderPayment = ref(false)
  const billDialog = ref(false)

  const importOrder = ref(false)
  const searchQuery = ref<string>('')

  // const receiptPayment = ref(false)
  const qrCode = ref(false)
  // const cash = ref(0)
  // const promotionSelected = ref<Promotion[]>([])
  const orderBill = ref<PurOrderBill[]>([])
  const orderDetails = ref<PurOrderDetail[]>([])
  const purOrderBill = ref<PurOrderBill>({
    total: 0,
    quantity: 0,
    user: authStore.getCurrentUser(),
    date: dayjs(new Date()).format('D/M/YYYY H:mm'),
    nameStore: 'Coffee',
    orderDetails: [],
    vender: null
  })

  // const initialOrder: PurOrderBill = {
  //   total: 0,
  //   quantity: 0,
  //   user: authStore.getCurrentUser(),
  //   date: new Date().toString(),
  //   nameStore: 'Coffee',
  //   orderDetails: [],
  //   vender: null
  // }
  // const editedPurOrder = ref<PurOrderBill>(JSON.parse(JSON.stringify(initialOrder)))

  function addPurOrderBills() {
    //////////////// SAVE  ///////////////////
    purOrderBill.value.orderDetails = orderDetails.value
    // purOrderBill.value.cash = cash.value

    const purOrder: PurOrder = {
      purOrderDetails: [],
      userId: authStore.getCurrentUser()?.id
    }

    for (let i = 0; i < orderDetails.value.length; i++) {
      const newOrderDetail = {
        materialId: orderDetails.value[i].material.id!,
        quantity: orderDetails.value[i].quantity
      }
      purOrder.purOrderDetails.push(newOrderDetail)
    }
    console.log('purOrder' + purOrder)

    purOrderService.addPurOrder(purOrder)
  }

  function addPurOrderDetail(material: Material) {
    const index = orderDetails.value.findIndex((item) => item.material.id === material.id)
    if (index >= 0) {
      orderDetails.value[index].quantity++
      calOrder()
      return
    } else {
      const newPurOrder: PurOrderDetail = {
        quantity: 1,
        material: material
      }
      orderDetails.value.push(newPurOrder)
    }
    calOrder()
  }
  function clearForm() {
    // editedPurOrder.value = JSON.parse(JSON.stringify(initialOrder))
    purOrderBill.value = {
      total: 0,
      quantity: 0,
      user: authStore.getCurrentUser(),
      date: new Date().toString(),
      nameStore: 'Coffee',
      orderDetails: [],
      vender: null
    }
  }
  function calOrder() {
    let totalBefore = 0
    for (const item of orderDetails.value) {
      totalBefore = totalBefore + item.material.price * item.quantity
      purOrderBill.value.total = totalBefore
      // editedPurOrder.value.total = totalBefore
    }
    // if (memberStore.currentMember) {
    //   receipt.value.total = totalBefore - totalBefore * 0.05
    // } else {
    //   receipt.value.total = totalBefore
    // }
  }

  async function deletePurOrder() {
    loadingStore.doLoading()
    const orders = purOrderBill.value
    // const orders = editedPurOrder.value
    const res = await purOrderService.delPurOrder(orders)
    await getPurOrders()
    loadingStore.finish()
  }

  function removeOrderDetail(orderDetail: PurOrderDetail) {
    const index = orderDetails.value.findIndex((item) => item === orderDetail)
    orderDetails.value.splice(index, 1)
    calOrder()
  }
  function increase(item: PurOrderDetail) {
    item.quantity++
    calOrder()
  }
  function decrease(item: PurOrderDetail) {
    if (item.quantity === 1) {
      removeOrderDetail(item)
    }
    item.quantity--
    calOrder()
  }

  function showImport() {
    importOrder.value = true
  }

  function showBillPayment() {
    if (orderDetails.value.length > 0) {
      purOrderPayment.value = true
    }
  }

  function showPurBillDialog() {
    addPurOrderBills()
    console.log(purOrderBill)

    // editedPurOrder.value.orderDetails = orderDetails.value
    purOrderBill.value.orderDetails = orderDetails.value
    console.log(purOrderBill.value.orderDetails)
    // console.log(editedPurOrder.value.orderDetails)
    purOrderDialog.value = true
  }

  async function showOrderDetial(id: number) {
    try {
      loadingStore.doLoading()
      const res = await purOrderService.getPurOrder(id)
      // editedPurOrder.value.orderDetails = res.data
      purOrderBill.value = res.data

      console.log(purOrderBill.value)
      billDialog.value = true
      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }
  function clear() {
    billDialog.value = false
    importOrder.value = false
    purOrderDialog.value = false
    qrCode.value = false
    // editedPurOrder.value = JSON.parse(JSON.stringify(initialOrder))
    orderDetails.value = []
    purOrderBill.value = {
      total: 0,
      quantity: 0,
      user: authStore.getCurrentUser(),
      date: new Date().toString(),
      nameStore: 'Coffee',
      orderDetails: [],
      vender: null
    }
    // venderStore.clear()
  }

  //////// promotion ///////////

  // function addPromotionItem(promotion: Promotion) {
  //   const index = promotionSelected.value.findIndex((item) => item.id === promotion.id)
  //   if (index >= 0) {
  //     return
  //   } else {
  //     promotionSelected.value.push(promotion)
  //   }
  // }

  async function getPurOrder(id: number) {
    try {
      loadingStore.doLoading()
      const res = await purOrderService.getPurOrder(id)
      orderBill.value = res.data
      // editedPurOrder.value = res.data

      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }

  async function getPurOrders() {
    try {
      loadingStore.doLoading()
      const res = await purOrderService.getPurOrders()
      orderBill.value = res.data

      loadingStore.finish()
    } catch (e) {
      console.log('Error')

      loadingStore.finish()
    }
  }

  const filteredPurOrder = computed(() => {
    const query = searchQuery.value.toLowerCase().trim()
    if (!query) {
      return orderBill.value // Return all materials if search query is empty
    } else {
      return orderBill.value.filter((order) => {
        // Filter based on date property
        return (
          order.date &&
          (order.date.toLowerCase().includes(query) || order.date.toString().includes(query))
        )
      })
      // Add more fields to search as needed
    }
  })

  return {
    importOrder,
    showImport,
    showOrderDetial,
    getPurOrders,
    getPurOrder,
    deletePurOrder,
    clearForm,
    searchQuery,
    filteredPurOrder,
    orderDetails,
    addPurOrderDetail,
    removeOrderDetail,
    increase,
    decrease,
    purOrderBill,
    calOrder,
    showPurBillDialog,
    purOrderDialog,
    clear,
    qrCode,
    showBillPayment,
    purOrderPayment,
    billDialog,
    addPurOrderBills
  }
})
