import type { Material } from '@/types/stock/Materials'
import { defineStore } from 'pinia'
import { useLoadingStore } from '../loading'
import { computed, ref } from 'vue'
import materialService from '@/services/stocks/material'
import material from '@/services/stocks/material'
import type { CheckDetail } from '@/types/stock/CheckDetail'

export const useMaterialStore = defineStore('material', () => {
  const loadingStore = useLoadingStore()
  const materials = ref<Material[]>([])
  const stock = ref<CheckDetail[]>([])
  const searchQuery = ref<string>('')
  const initialMaterial: Material = {
    name: '',
    in_date: '',
    price: 0,
    qt_previous: 0,
    quantity: 0,
    min: 0,
    use: 0,
    status: 'Low'
  }
  const editedMaterial = ref<Material & { files: File[] }>(
    JSON.parse(JSON.stringify(initialMaterial))
  )
  const initiaStockDetail: CheckDetail = {
    name: editedMaterial.value.name,
    in_date: editedMaterial.value.in_date,
    price: editedMaterial.value.price,
    qt_previous: editedMaterial.value.quantity,
    quantity: 0,
    min: editedMaterial.value.min,
    use: editedMaterial.value.use,
    status: 'Low',
    materialId: editedMaterial.value.id!,
    material: editedMaterial.value
  }

  const editedStockDetail = ref<CheckDetail>(JSON.parse(JSON.stringify(initiaStockDetail)))
  async function getMaterials() {
    try {
      loadingStore.doLoading()
      const res = await materialService.getMaterials()
      materials.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('Error')

      loadingStore.finish()
    }
  }

  async function getMaterial(id: number) {
    try {
      loadingStore.doLoading()
      const res = await materialService.getMaterial(id)
      editedMaterial.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }

  async function saveMaterial() {
    try {
      loadingStore.doLoading()
      const material = editedMaterial.value
      if (!material.id) {
        // add new
        console.log('post ' + JSON.stringify(material))
        const res = await materialService.addMaterial(material)
      } else {
        // update
        console.log('patch ' + JSON.stringify(material))
        const res = await materialService.updateMaterial(material)
      }
      await getMaterials()
      loadingStore.finish()
    } catch (e: any) {
      //  messageStore.showMessage(e.message)
      console.log('Error')
      loadingStore.finish()
    }
  }

  async function deleteMaterial() {
    loadingStore.doLoading()
    const material = editedMaterial.value
    const res = await materialService.delMaterial(material)

    await getMaterials()
    loadingStore.finish()
  }

  function clearForm() {
    editedMaterial.value = JSON.parse(JSON.stringify(initialMaterial))
  }

  function getColor(status: string) {
    if (status === 'Low') return 'orange'
    else return 'green'
  }

  const filteredMaterials = computed(() => {
    const query = searchQuery.value.toLowerCase().trim()
    if (!query) {
      return materials.value // Return all materials if search query is empty
    } else {
      return materials.value.filter(
        (material) =>
          material.name.toLowerCase().includes(query) || material.price.toString().includes(query)
        // Add more fields to search as needed
      )
    }
  })
  // const filteredStock = computed(() => {
  //   const query = searchQuery.value.toLowerCase().trim()
  //   if (!query) {
  //     return stock.value // Return all materials if search query is empty
  //   } else {
  //     return stock.value.filter(
  //       (stock) =>
  //         stock.name.toLowerCase().includes(query) || stock.price.toString().includes(query)
  //       // Add more fields to search as needed
  //     )
  //   }
  // })
  return {
    materials,
    initialMaterial,
    editedMaterial,
    editedStockDetail,
    getMaterials,
    saveMaterial,
    deleteMaterial,
    getMaterial,
    clearForm,
    getColor,
    filteredMaterials,
    searchQuery
  }
})
