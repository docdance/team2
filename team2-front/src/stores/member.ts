import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useReceiptPromotion } from './receiptPromotion'
import { useLoadingStore } from './loading'
import memberService from '@/services/member'
// import { useReceiptStore } from './receipt'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    { id: 1, name: 'Kittiphong Namwong', tel: '1', point: 500 },
    { id: 2, name: 'Ornwitchaya Jusombut', tel: '2', point: 0 },
    { id: 3, name: 'Sirapatsorn Chada', tel: '3', point: 0 },
    { id: 4, name: 'Sahaphap Ritnetikul', tel: '4', point: 0 }
  ])
  const isMember = ref(false)
  const loadingStore = useLoadingStore()
  const lastId = ref(5)
  const currentMember = ref<Member | null>()
  currentMember.value = null
  const showMember = ref(false)

  const searchMember = async (tel: string) => {
    loadingStore.doLoading()
    const res = await memberService.getMember(tel)
    currentMember.value = res.data
    loadingStore.finish()
    isMember.value = true
  }

  function addMember(name: string, tel: string) {
    const newMember = ref<Member>({
      id: lastId.value++,
      name: name,
      tel: tel,
      point: 0
    })
    members.value.push(newMember.value)
  }

  function clear() {
    showMember.value = false
    isMember.value = false
    currentMember.value = null
  }
  return { members, currentMember, searchMember, clear, showMember, addMember, isMember }
})
