import { ref } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/Receipt/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import type { Order } from '@/types/Receipt/Order'
import { useReceiptPromotion } from './receiptPromotion'
import { useOrderStore } from './order'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const orderStore = useOrderStore()
  const receiptPromotion = useReceiptPromotion()
  const receiptDialog = ref(false)
  const receiptPayment = ref(false)
  const qrCode = ref(false)
  const cash = ref(0)
  const gotPoint = ref(0)
  const usePoint = ref(0)
  const receiptItems = ref<ReceiptItem[]>([])
  const receipt = ref<Receipt>({
    total: 0,
    qty: 0,
    created: new Date(),
    receiptItems: [],
    user: authStore.getCurrentUser(),
    cash: 0,
    discount: 0,
    member: null,
    payment: 'cash'
  })

  function addReceiptBills() {
    //////////////// SAVE RECEIPT ///////////////////
    receipt.value.receiptItems = receiptItems.value
    receipt.value.cash = cash.value
    if (memberStore.currentMember) {
      gotPoint.value += Math.floor(receipt.value.total / 10)
      memberStore.currentMember.point -= usePoint.value
      memberStore.currentMember.point += gotPoint.value
      receipt.value.member = memberStore.currentMember
    }

    const order: Order = {
      orderItems: [],
      userId: authStore.getCurrentUser()?.id,
      cash: receipt.value.cash,
      discount: receipt.value.discount,
      promotions: receiptPromotion.promotionSelected,
      member: receipt.value.member,
      getPoint: gotPoint.value,
      payment: receipt.value.payment,
      usePoint: usePoint.value
    }

    for (let i = 0; i < receiptItems.value.length; i++) {
      const newOrderItem = {
        productId: receiptItems.value[i].product.id!,
        qty: receiptItems.value[i].qty
      }
      order.orderItems.push(newOrderItem)
    }
    console.log('Receipt' + order)

    orderStore.saveOrder(order)
  }

  ////////////////// Receipt Items //////////////////////////
  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].qty++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        qty: 1,
        product: product
      }
      receiptItems.value.push(newReceipt)
    }
    calReceipt()
  }

  ////////////////// Calculate Receipt //////////////////////////
  function calReceipt() {
    gotPoint.value = 0
    usePoint.value = 0
    let totalBefore = 0
    let promotionDiscount = 0
    receipt.value.qty = 0
    //Cal totalBefor
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.product.price * item.qty
      receipt.value.qty += item.qty
    }
    //Cal total from Promotion
    // for (const item of receiptPromotion.promotionSelected) {
    //   if (item.forItem) {
    //     if (item.minQty > 0 || item.minPrice > 0) {
    //       if (item.productId >= 0) {
    //         const receiptItemIndex = receiptItems.value.findIndex(
    //           (itemP) => itemP.product.id === item.productId
    //         )
    //         if (item.percentDiscount > 0) {
    //           const qtyDiscount = Math.floor(receiptItems.value[receiptItemIndex].qty / item.minQty)
    //           promotionDiscount +=
    //             ((receiptItems.value[receiptItemIndex].product.price * item.percentDiscount) /
    //               100) *
    //             qtyDiscount
    //         } else if (item.priceDiscount > 0) {
    //           const qtyDiscount = Math.floor(receiptItems.value[receiptItemIndex].qty / item.minQty)
    //           promotionDiscount += item.priceDiscount * qtyDiscount
    //         }
    //       }
    //     }
    //   }
    //   if (item.forOder) {
    //     console.log('Totoal before1 :', totalBefore)
    //     if (item.percentDiscount > 0) {
    //       promotionDiscount += (totalBefore * item.percentDiscount) / 100
    //     } else if (item.priceDiscount > 0) {
    //       console.log('Totoal before2 :', totalBefore)
    //       promotionDiscount += item.priceDiscount
    //     }
    //   }
    // }

    for (const item of receiptPromotion.promotionSelected) {
      if (item.forOneItem === false) {
        // console.log('Total before1: ', totalBefore)
        if (item.percentDiscount > 0) {
          promotionDiscount += (totalBefore * item.percentDiscount) / 100
        } else if (item.priceDiscount > 0) {
          // console.log('Total before2: ', totalBefore)
          promotionDiscount += item.priceDiscount
        }
      }

      if (
        item.forOneItem === true &&
        (item.minQty > 0 || item.minPrice > 0) &&
        item.productId >= 0
      ) {
        // console.log('Total before3: ', totalBefore)
        const receiptItemIndex = receiptItems.value.findIndex(
          (itemP) => itemP.product.id === item.productId
        )
        if (receiptItemIndex !== -1) {
          const qtyDiscount = Math.floor(receiptItems.value[receiptItemIndex].qty / item.minQty)
          if (item.percentDiscount > 0) {
            // console.log(receiptItems.value[receiptItemIndex].qty, item.minQty, qtyDiscount)
            // for loop for increase promotion by qty condition
            for (let i = 0; i < qtyDiscount; i++) {
              promotionDiscount +=
                ((receiptItems.value[receiptItemIndex].product.price * item.percentDiscount) /
                  100) *
                item.minQty
            }

            // console.log('qtyDiscount', qtyDiscount)
          } else if (item.priceDiscount > 0) {
            promotionDiscount += item.priceDiscount * qtyDiscount
          }
        }
      }
      if (item.forOneItem === false && memberStore.currentMember) {
        if (item.getPoint > 0) {
          gotPoint.value += item.getPoint
          console.log('1')
        }
        if (item.usePoint > 0) {
          usePoint.value += item.usePoint
        }
      }
    }
    console.log(promotionDiscount)
    receipt.value.discount = promotionDiscount

    receipt.value.total = totalBefore - promotionDiscount
  }

  //////////////////////// Remove Items ////////////////////////////
  function removeReceioptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function increase(item: ReceiptItem) {
    item.qty++
    calReceipt()
  }
  function decrease(item: ReceiptItem) {
    if (item.qty === 1) {
      removeReceioptItem(item)
    }
    item.qty--
    calReceipt()
  }

  function showReceiptPayment() {
    if (receiptItems.value.length > 0) {
      receiptPayment.value = true
    }
  }

  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    // console.log(receipt.value.receiptItems)
    receiptDialog.value = true
  }

  //////////////////////// Clear Receipt ////////////////////////////
  function clear() {
    cash.value = 0
    gotPoint.value = 0
    receiptDialog.value = false
    qrCode.value = false
    receiptItems.value = []
    receipt.value = {
      total: 0,
      qty: 0,
      created: new Date(),
      receiptItems: [],
      user: authStore.getCurrentUser(),
      cash: 0,
      discount: 0,
      member: null,
      payment: 'cash'
      // change: 0,
      // paymentType: 'cash',
      // memberDiscount: 0,
    }
    memberStore.clear()
    receiptPromotion.clearPromotionSelect()
  }

  return {
    receiptItems,
    addReceiptItem,
    removeReceioptItem,
    increase,
    decrease,
    receipt,
    calReceipt,
    showReceiptDialog,
    receiptDialog,
    clear,
    showReceiptPayment,
    receiptPayment,
    qrCode,
    cash,
    gotPoint,
    addReceiptBills
  }
})
