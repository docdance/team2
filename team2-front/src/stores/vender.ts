import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Vender } from '@/types/Vender'
import { useLoadingStore } from './loading'
import venderService from '@/services/vender'
import { useMessageStore } from './message'

export const useVenderStore = defineStore('vender', () => {
  const venders = ref<Vender[]>([])
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const showVender = ref(false)
  const currentVender = ref<Vender | null>()

  const initialVender: Vender & { files: File[] } = {
    name: '',
    tel: '',
    image: '',
    files: []
  }
  const editedVender = ref<Vender & { files: File[] }>(JSON.parse(JSON.stringify(initialVender)))

  async function getVenders() {
    try {
      loadingStore.doLoading()
      const res = await venderService.getVenders()
      venders.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('Error')

      loadingStore.finish()
    }
  }

  // async function addVender(name: string, tel: string) {
  //   try {
  //     loadingStore.doLoading()
  //     editedVender.value.name = name
  //     editedVender.value.tel = tel
  //     const vender = editedVender.value

  //     console.log('Post ' + JSON.stringify(vender))
  //     const res = await venderService.addVender(vender)

  //     await getVenderByName(name)
  //     loadingStore.finish()
  //   } catch (e: any) {
  //     messageStore.showMessage(e.message)
  //     loadingStore.finish()
  //   }
  // }

  async function getVender(id: number) {
    loadingStore.doLoading()
    const res = await venderService.getVender(id)
    editedVender.value = res.data
    loadingStore.finish()
  }

  const getVenderByTel = (tel: string): Vender | null => {
    for (const vender of venders.value) {
      if (vender.tel === tel) return vender
    }
    return null
  }

  const getVenderByName = (name: string): Vender | null => {
    for (const vender of venders.value) {
      if (vender.name === name) return vender
    }
    return null
  }

  const searchVender = (name: string) => {
    const index = venders.value.findIndex((item) => item.name === name)
    if (index < 0) {
      currentVender.value = null
    }
    currentVender.value = venders.value[index]
  }
  function addVender(name: string, tel: string) {
    const newVender = ref<Vender>({
      name: name,
      tel: tel,
      image: ''
    })
    venders.value.push(newVender.value)
  }

  function clear() {
    showVender.value = false
    currentVender.value = null
  }
  return {
    venders,
    showVender,
    getVenderByTel,
    getVenders,
    getVender,
    getVenderByName,
    addVender,
    clear,
    searchVender,
    currentVender
  }
})
