import { useLoadingStore } from './loading'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import productService from '@/services/product'
import type { Role } from '@/types/Role'
import { useMessageStore } from './message'
import type { Product } from '@/types/Product'
import type { Type } from '@/types/Type'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const products = ref<Product[]>([])
  const type = ref<Type[]>([])
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0,
    type: { id: 2, name: 'drink' },
    image: 'noImage.jpg',
    files: []
  }

  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))

  const getProductById = async (id: number) => {
    const res = productService.getProduct(id)
    editedProduct.value = (await res).data
    return editedProduct.value
  }

  async function getProduct(id: number) {
    try {
      loadingStore.doLoading()
      const res = await productService.getProduct(id)
      editedProduct.value = res.data
      loadingStore.finish()
    } catch (error: any) {
      messageStore.showMessage(error.message)
      loadingStore.finish()
    }
  }

  async function getProducts() {
    loadingStore.doLoading()
    const res = await productService.getProducts()
    products.value = res.data
    loadingStore.finish()
  }

  async function saveProduct() {
    try {
      /////////////// try catch to defind problem
      loadingStore.doLoading()
      ///////////
      console.log(editedProduct.value)
      const product = editedProduct.value
      if (!product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.addProduct(product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.updateProduct(product)
      }

      await getProducts()
      loadingStore.finish()
    } catch (error: any) {
      messageStore.showMessage(error.message) //show error message
      loadingStore.finish()
    }
  }
  async function deleteProduct() {
    try {
      loadingStore.doLoading()
      const product = editedProduct.value
      const res = await productService.delProduct(product)

      await getProducts()
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
  }

  //getType
  async function getType() {
    const res = await productService.getType()
    type.value = res.data
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }
  return {
    products,
    getProducts,
    saveProduct,
    deleteProduct,
    editedProduct,
    getProductById,
    getProduct,
    clearForm,
    getType,
    type
  }
})
