import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import orderService from '@/services/order'
import type { Order } from '@/types/Receipt/Order'

export const useOrderStore = defineStore('order', () => {
  const loadingStore = useLoadingStore()
  const orders = ref<Order[]>([])

  type OrderItems = {
    productId: number
    qty: number
  }

  const initialOrder: Order = {
    orderItemsIdQty: [],
    cash: 0,
    discount: 0,
    promotions: [],
    member: null,
    payment: 'cash',
    getPoint: 0,
    usePoint: 0,
    created: new Date(),
    total: 0,
    qty: 0
  }
  const editedOrder = ref<Order>(JSON.parse(JSON.stringify(initialOrder)))

  async function getOrder(id: number) {
    loadingStore.doLoading()
    const res = await orderService.getOrder(id)
    editedOrder.value = res.data
    editedOrder.value.orderItemsIdQty = []
    for (let i = 0; i < res.data.orderItems.length; i++) {
      const getOrderItem: OrderItems = {
        productId: res.data.orderItems.product.id,
        qty: res.data.orderItems.qty
      }
      editedOrder.value.orderItemsIdQty.push(getOrderItem)
    }

    // editedOrder.value.cash = res.data.cash
    // editedOrder.value.discount= res.data.discount
    // editedOrder.value.promotions= res.data.promotions
    // editedOrder.value.member= res.data.member
    // editedOrder.value.payment= res.data.payment
    // editedOrder.value.getPoint= res.data.getPoint
    // editedOrder.value.usePoint= res.data.usePoint
    // editedOrder.value.created= res.data.created
    // editedOrder.value.total= res.data.
    // editedOrder.value.qty= res.data.
    loadingStore.finish()
  }

  async function getOrders() {
    try {
      loadingStore.doLoading()
      const res = await orderService.getOrders()
      orders.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveOrder(order: Order) {
    loadingStore.doLoading()
    const res = await orderService.addOrder(order)
    await getOrders()
    loadingStore.finish()
  }
  async function deleteOrder() {
    loadingStore.doLoading()
    const order = editedOrder.value
    const res = await orderService.delOrder(order)
    await getOrders()
    loadingStore.finish()
  }

  function clearForm() {
    editedOrder.value = JSON.parse(JSON.stringify(initialOrder))
  }
  return {
    orders,
    getOrders,
    saveOrder,
    deleteOrder,
    editedOrder,
    getOrder,
    clearForm,
    initialOrder
  }
})
