import type { User } from '@/types/User'
import http from './http'

function addUser(user: User & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', user.email)
  formData.append('fullName', user.fullName)
  formData.append('gender', user.gender)
  formData.append('password', user.password)
  formData.append('tel', user.tel)
  formData.append('baseSalary', String(user.baseSalary))
  formData.append('bankName', user.bankName)
  formData.append('bankAccount', user.bankAccount)
  // formData.append('salaryStatus', user.salaryStatus)
  formData.append('roles', JSON.stringify(user.roles))
  if (user.files && user.files.length > 0) formData.append('file', user.files[0])
  return http.post('/users', formData, { headers: { 'Content-Type': 'multipart/form-data' } })
}

function updateUser(user: User & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', user.email)
  formData.append('fullName', user.fullName)
  formData.append('gender', user.gender)
  formData.append('password', user.password)
  formData.append('tel', user.tel)
  formData.append('baseSalary', String(user.baseSalary))
  formData.append('bankName', user.bankName)
  formData.append('bankAccount', user.bankAccount)
  // formData.append('salaryStatus', user.salaryStatus)
  formData.append('roles', JSON.stringify(user.roles))
  if (user.files && user.files.length > 0) formData.append('file', user.files[0])
  return http.post(`/users/${user.id}`, formData, {
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}

function delUser(user: User) {
  return http.delete(`/users/${user.id}`)
}

function getUser(id: number) {
  return http.get(`/users/${id}`)
}

function getUsers() {
  return http.get('/users', {
    headers: { Authorization: 'Bearer' + localStorage.getItem('access_token') }
  })
}

export default { addUser, updateUser, delUser, getUser, getUsers }
