import type { Vender } from '@/types/Vender'
import http from './http'

function addVender(vender: Vender & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', vender.name)
  formData.append('tel', vender.tel)

  if (vender.files && vender.files.length > 0) formData.append('file', vender.files[0])
  return http.post('/venders', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateVender(vender: Vender & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', vender.name)
  formData.append('tel', vender.tel)

  if (vender.files && vender.files.length > 0) formData.append('file', vender.files[0])
  return http.post(`/venders/${vender.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delVender(vender: Vender) {
  return http.delete(`/venders/${vender.id}`)
}

function getVender(id: number) {
  return http.get(`/venders/${id}`)
}

function getVenders() {
  return http.get('/venders')
}

export default { addVender, updateVender, delVender, getVender, getVenders }
