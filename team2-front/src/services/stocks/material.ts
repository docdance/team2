import type { Material } from '@/types/stock/Materials'
import http from '../http'

// function addMaterial(material: Material) {
//   const formData = new FormData()
//   formData.append('name', material.name)
//   formData.append('price', material.price.toString())
//   formData.append('quantity', material.price.toString())
//   formData.append('min', material.min.toString())
//   formData.append('status', material.status)
//   // if (material.files && material.files.length > 0) formData.append('file', material.files[0])
//   return http.post('/materials', formData, {
//     headers: {
//       'Content-Type': 'multipart/form-data'
//     }
//   })
// }

// function updateMaterial(material: Material) {
//   const formData = new FormData()
//   formData.append('name', material.name)
//   formData.append('price', material.price.toString())
//   formData.append('quantity', material.price.toString())
//   formData.append('min', material.min.toString())
//   formData.append('status', material.status)
//   // if (material.files && material.files.length > 0) formData.append('file', material.files[0])
//   return http.patch(`/materials/${material.id}`, formData, {
//     headers: {
//       'Content-Type': 'multipart/form-data'
//     }
//   })
// }

function addMaterial(Material: Material) {
  return http.post('/materials', Material)
}

function updateMaterial(Material: Material) {
  return http.patch(`/materials/${Material.id}`, Material)
}

function delMaterial(Material: Material) {
  return http.delete(`/materials/${Material.id}`)
}

function getMaterial(id: number) {
  return http.get(`/materials/${id}`)
}

function getMaterials() {
  return http.get('/materials')
}

// function getMaterialByStatus(status: string) {
//   return http.get(`/materials/${status}`)
// }

export default { addMaterial, updateMaterial, delMaterial, getMaterial, getMaterials }
