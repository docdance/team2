import http from '@/services/http'
import type { PurOrder } from '@/types/stock/PurOrder'
import type { PurOrderBill } from '@/types/stock/PurOrderBill'

function addPurOrder(purOrder: PurOrder) {
  console.log(purOrder)

  return http.post('/materialOrders', purOrder)
}
function getPurOrders() {
  return http.get('/materialOrders')
}

function getPurOrder(id: number) {
  return http.get(`/materialOrders/${id}`)
}

function delPurOrder(purOrder: PurOrderBill) {
  return http.delete(`/materialOrders/${purOrder.id}`)
}

export default { addPurOrder, getPurOrders, getPurOrder, delPurOrder }
