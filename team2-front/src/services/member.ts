import http from './http'
import type { Member } from '@/types/Member'

async function addMember(member: Member) {
  const res = await http.post('/members', member)
}

async function updateMember(promotion: Member) {
  const res = await http.patch('/members/' + promotion.id, promotion)
}

async function delMember(promotion: Member) {
  const res = await http.delete('/members/' + promotion.id)
}

function getMember(tel: string) {
  return http.get('/members/' + tel)
}

function getMembers() {
  return http.get('/members')
}

export default { addMember, updateMember, delMember, getMember, getMembers }
