import http from './http'
import type { Promotion } from '@/types/Promotion'
import type { PromotionAdd } from '@/types/PromotionAdd'

async function addPromotion(promotionAdd: PromotionAdd) {
  const res = await http.post('/promotions', promotionAdd)
}

async function updatePromotion(promotion: Promotion) {
  const res = await http.patch('/promotions/' + promotion.id, promotion)
}

async function delPromotion(promotion: Promotion) {
  const res = await http.delete('/promotions/' + promotion.id)
}

function getPromotion(id: number) {
  return http.get('/promotions/' + id)
}

function getPromotions() {
  return http.get('/promotions')
}

export default { addPromotion, updatePromotion, delPromotion, getPromotion, getPromotions }
